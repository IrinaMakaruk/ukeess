import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../environments/environment';
import { Employee, Department } from '../models';

import { handleError } from '../helpers/helpers';

import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class EmployeesService {
	constructor(private http: HttpClient) { }

	getAllEmployees(page: number) {
		return this.http.get<Employee[]>(`${environment.apiUrl}/employees/pagination/${page}`).pipe(catchError(handleError));
	}

	getEmployeesCount() {
		return this.http.get<number>(`${environment.apiUrl}/employees/count`).pipe(catchError(handleError));
	}

	editEmployee( employee: Employee) {
		return this.http.put<Employee>(`${environment.apiUrl}/employees/${employee.empId}`, employee).pipe(catchError(handleError));
	}

	deleteEmployee( id: string) {
		return this.http.delete<string>(`${environment.apiUrl}/employees/${id}`).pipe(catchError(handleError));
	}

	addEmployee(employee: Employee) {
		return this.http.post<Employee>(`${environment.apiUrl}/employees/add`, employee).pipe(catchError(handleError));
	}

	searchEmployee(searchParam: string) {
		return this.http.get<any>(`${environment.apiUrl}/employees/search/${searchParam}`,).pipe(catchError(handleError));
	}
	
	getDepartments() {
		return this.http.get<Department[]>(`${environment.apiUrl}/employees/departments`).pipe(catchError(handleError));
	}
}