import { Component } from '@angular/core';
import { AuthenticationService } from 'src/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  isLoggedIn: boolean = false;
  title = 'angular-sql-crud';

  constructor( private authenticationService: AuthenticationService) { 
    this.isLoggedIn =  this.authenticationService.isLoggedIn; 
  }

  logout() {
    this.authenticationService.logout();
  }
  
}
