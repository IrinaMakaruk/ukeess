import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services';

import { User } from 'src/models';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading:boolean = false;
  submitted: boolean = false;
  error:string = '';
  signUp: boolean = false;
  user: User = {
    username: '',
    password: ''
  };

  constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService
  ){ 
    this.authenticationService.isLoggedIn && this.router.navigate(['home']); 
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['']
    });
  }

  get formData() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) return ; 

    this.loading = true;
    const auth = this.signUp ?
      this.authenticationService.signUp(this.loginForm.value)
      : this.authenticationService.login(this.formData.username.value, this.formData.password.value);
    
      auth.subscribe(
        _data => this.loading = false,
        error => {
          this.error = error;
          this.loading = false;
      });
  }

  openSignUp() {
    this.signUp = !this.signUp;
    this.loginForm.controls['email'].setValidators([Validators.required, Validators.email])  
  }
}
