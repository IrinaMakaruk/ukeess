import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

import { MatDialog } from '@angular/material/dialog';

import { first } from 'rxjs/operators';

import { Employee, Department } from './../../models';
import { EmployeesService } from './../../services';

import { AddEditEmployeeDialog } from './add-edit-employee-dialog/add-edit-employee-dialog.component';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html'
})

export class EmployeesComponent implements OnInit {
  loading = false;
  employeesCount: number = 0;
  displayedColumns: string[] = ['empId', 'empName', 'empActive', 'empDpID', 'actions']
  dataSource:Employee[] = [];
  defaultPage:number = 1;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @Output() page: EventEmitter<PageEvent>;
  
  constructor(
    private employeesService: EmployeesService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loading = true;
    this.getEmployeesCount();
    this.getEmployeesList(this.defaultPage);
  }

  navigatePage() {
    const nextPageNumber = this.paginator.pageIndex + 1;
    this.getEmployeesList(nextPageNumber);
  }

  private getEmployeesCount() {
    this.employeesService.getEmployeesCount().subscribe(count => {
      this.employeesCount = count;
    });
  }

  private getEmployeesList( page: number ) {
    this.employeesService.getAllEmployees(page).pipe(first()).
      subscribe(employees => {
        this.loading = false;
        this.dataSource = employees;
    });
  }

  editEmployee(employee: Employee) {
    this.openEmployeeModal(employee);
  }

  addNewEmployee() {
    this.openEmployeeModal();
  }

  deleteEmployee( {empId} ) {
    this.employeesService.deleteEmployee( empId ).subscribe(_data => {
      this.updateTable();
    });;
  }

  searchEmployee(searchParam?: string) {
    this.employeesService.searchEmployee( searchParam ).subscribe(employee => {
      this.dataSource = searchParam ? employee : this.getEmployeesList(this.paginator.pageIndex + 1);;
    });
  }
  private openEmployeeModal(employee?: Employee) {
    const dialogRef = this.dialog.open(AddEditEmployeeDialog, {
      width: '600px',
      data: employee
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateTable();
    });
  }

  private updateTable() {
    this.getEmployeesCount();
    this.getEmployeesList(this.paginator.pageIndex + 1);
  }
}
