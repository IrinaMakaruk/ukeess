import { Component,OnInit, Inject } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Employee } from 'src/models';
import { EmployeesService } from 'src/services/employees.service';

@Component({
  selector: 'add-edit-employee-dialog',
  templateUrl: './add-edit-employee-dialog.component.html'
})

export class AddEditEmployeeDialog implements OnInit {

  employeeForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  error: string;
  employeeData: Employee;
  departments = [];

  constructor(
    private formBuilder: FormBuilder,
    private employeesService: EmployeesService,
    public dialogRef: MatDialogRef<AddEditEmployeeDialog>,

    @Inject(MAT_DIALOG_DATA) public data: Employee) {
      this.employeeData = data;
    }

    ngOnInit() {
      this.getDepartments();
      this.setFormData();
    }

    get formData() { return this.employeeForm.controls; }

    onSubmit() {
      this.submitted = true;
      const defaultDepartment = this.departments[0].dpID;
      
      if (this.employeeForm.invalid) return; 

      const employee: Employee = {
        empName: this.formData.name.value,
        empActive: this.formData.active.value,
        empDpID: this.formData.department.value || defaultDepartment,
        empId: this.employeeData.empId
      };

      this.loading = true;
      const saveEmployeeObserver = Object.keys(this.employeeData).length > 0
        ? this.employeesService.editEmployee(employee)
        : this.employeesService.addEmployee(employee);
      
        saveEmployeeObserver.subscribe(
          _data => this.loading = false,
          error => {
            this.error = error;
            this.loading = false;
          });
        this.dialogRef.close();
    }

    private getDepartments() {
      this.employeesService.getDepartments().subscribe( departments => {
        this.departments = departments;
      })
    }

    private setFormData() {
      const editEmployeeMode = Object.keys(this.employeeData).length > 0;
      this.employeeForm = this.formBuilder.group({
        name: [ editEmployeeMode ? this.employeeData.empName : '', Validators.required],
        active: [editEmployeeMode ? this.employeeData.empActive: false, Validators.required],
        department: [editEmployeeMode ? this.employeeData.empDpID : '', Validators.required]
      });
    }
}
