import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from './employees/employees.component';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from '../helpers/auth-guard';

const routes: Routes = [
  { path: 'login', component:  LoginComponent },
  { path: 'home', component: EmployeesComponent, canActivate: [AuthGuard]},

  // otherwise redirect to login
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
