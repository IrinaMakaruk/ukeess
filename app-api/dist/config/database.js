"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = require("mysql");
function connect() {
    return __awaiter(this, void 0, void 0, function* () {
        return mysql_1.createPool({
            host: 'localhost',
            user: 'root',
            database: 'sql_app',
            connectionLimit: 100,
            typeCast: (field, next) => {
                if (field.type === 'BIT' && field.length == 1) {
                    return (field.buffer()[0] === 1);
                }
                return next();
            }
        });
    });
}
exports.connect = connect;
//# sourceMappingURL=database.js.map