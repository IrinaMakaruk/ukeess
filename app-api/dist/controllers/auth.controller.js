"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const database_1 = require("../config/database");
const jwt_1 = require("../middlware/jwt");
const notLogged = (username, password) => !username && !password;
function Registration(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield database_1.connect();
            connection.query(`SELECT * FROM sql_app.Accounts WHERE LOWER(username) = LOWER(
            ${connection.escape(req.body.username)});`, (err, result) => {
                if (result.length) {
                    return res.status(409).send({
                        msg: 'This username is already in use!'
                    });
                }
                else {
                    // username is available
                    bcryptjs_1.default.hash(req.body.password, 10, (err, hash) => {
                        if (err) {
                            return res.status(500).send({ msg: "Error happend on the server. Please try later" });
                        }
                        else {
                            // has hashed pw => add to database
                            connection.query(`INSERT INTO sql_app.Accounts (username, email, password) VALUES (
                            ${connection.escape(req.body.username)},
                            ${connection.escape(req.body.email)},
                            ${connection.escape(hash)})`, (err, result) => {
                                if (err)
                                    return res.status(400).send({ msg: err });
                                const loggedUser = {
                                    username: req.body.username,
                                    password: req.body.password
                                };
                                const token = jwt_1.generateAccessToken(loggedUser);
                                return res.status(201).send({
                                    msg: 'Registered!',
                                    token,
                                    user: loggedUser
                                });
                            });
                        }
                    });
                }
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.Registration = Registration;
function Auth(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield database_1.connect();
            const { username, password } = req.body;
            if (notLogged(username, password))
                return res.status(401).send({ msg: 'User not logged!' });
            connection.query(`SELECT * FROM sql_app.Accounts WHERE username = ${connection.escape(username)}`, (error, results) => {
                // user does not exists
                if (error) {
                    return res.status(400).send({
                        msg: error
                    });
                }
                if (!results.length) {
                    return res.status(401).send({
                        msg: 'Username or password is incorrect!'
                    });
                }
                // check password
                const loggedUser = results[0];
                bcryptjs_1.default.compare(password, loggedUser.password, (loginError, loginResult) => {
                    // wrong password
                    if (loginError) {
                        return res.status(401).send({
                            msg: 'Username or password is incorrect!'
                        });
                    }
                    if (loginResult) {
                        const token = jwt_1.generateAccessToken({ username: loggedUser.username, password: loggedUser.password });
                        return res.status(200).send({
                            msg: 'Logged in!',
                            token,
                            user: loggedUser
                        });
                    }
                    return res.status(401).send({
                        msg: 'Username or password is incorrect!'
                    });
                });
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.Auth = Auth;
//# sourceMappingURL=auth.controller.js.map