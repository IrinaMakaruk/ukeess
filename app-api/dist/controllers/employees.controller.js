"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../config/database");
function getEmployees(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield database_1.connect();
            const pageNumber = Number(req.params.page);
            const offset = 10 * (pageNumber - 1);
            connection.query('SELECT sql_app.Employees.*, sql_app.Departments.dpName from sql_app.Employees INNER JOIN sql_app.Departments ON sql_app.Employees.empDpID = sql_app.Departments.dpID ORDER BY empId LIMIT 10 OFFSET ?', [offset], (err, employees) => {
                if (err)
                    throw err;
                res.json(employees);
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.getEmployees = getEmployees;
function addEmployee(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const newEmployee = req.body;
            const connection = yield database_1.connect();
            connection.query('INSERT INTO sql_app.Employees SET ?', [newEmployee], err => {
                if (err)
                    throw err;
                res.json({
                    message: 'new Employee added'
                });
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.addEmployee = addEmployee;
function searchEmployee(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const searchParam = req.params.searchParam;
            const connection = yield database_1.connect();
            connection.query('SELECT sql_app.Employees.*, sql_app.Departments.dpName from sql_app.Employees JOIN sql_app.Departments ON sql_app.Employees.empDpID = sql_app.Departments.dpID WHERE empName like ? ORDER BY empId', [`${searchParam}%`], (err, employees) => {
                if (err)
                    throw err;
                res.send(employees);
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.searchEmployee = searchEmployee;
function deleteEmployee(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const id = req.params.empId;
            const connection = yield database_1.connect();
            connection.query('DELETE FROM sql_app.Employees WHERE empId = ?', [id], (err) => {
                if (err)
                    throw err;
                res.json({
                    message: 'Employee Deleted'
                });
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.deleteEmployee = deleteEmployee;
function updateEmployee(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const id = req.params.empId;
            const updateEmployee = req.body;
            const connection = yield database_1.connect();
            connection.query('UPDATE sql_app.Employees SET ? WHERE empId = ?', [updateEmployee, id], (err) => {
                if (err)
                    throw err;
                res.json({
                    message: 'Employee Updated'
                });
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.updateEmployee = updateEmployee;
function getEmployeesCount(_req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield database_1.connect();
            connection.query('SELECT COUNT(DISTINCT empId) AS numbers FROM sql_app.Employees', (err, count) => {
                if (err)
                    throw err;
                res.json(count[0].numbers);
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.getEmployeesCount = getEmployeesCount;
function getDepartments(_req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const connection = yield database_1.connect();
            connection.query('SELECT * FROM sql_app.Departments', (err, departments) => {
                if (err)
                    throw err;
                res.json(departments);
            });
        }
        catch (e) {
            console.error(e);
        }
    });
}
exports.getDepartments = getDepartments;
//# sourceMappingURL=employees.controller.js.map