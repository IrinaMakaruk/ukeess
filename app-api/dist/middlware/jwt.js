"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.secret = 'secretkey23456';
function generateAccessToken(user) {
    return jsonwebtoken_1.default.sign(user, exports.secret);
}
exports.generateAccessToken = generateAccessToken;
function authenticateToken(req, res, next) {
    try {
        const authHeader = req.headers['authorization'];
        const token = authHeader && authHeader.split(' ')[1];
        if (token == null) {
            return res.sendStatus(401);
        }
        const decoded = jsonwebtoken_1.default.verify(token, exports.secret);
        req.user = decoded;
        res.cookie('SESSIONID', token, { httpOnly: true, secure: true });
        next();
    }
    catch (err) {
        return res.status(401).send({
            msg: 'You are not logged in. Please login again'
        });
    }
}
exports.authenticateToken = authenticateToken;
//# sourceMappingURL=jwt.js.map