"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const routes_1 = require("./routes");
class App {
    constructor(port) {
        this.port = port;
        this.app = express_1.default();
        this.app.use(cors_1.default());
        this.setAppSettings();
        this.routes();
    }
    setAppSettings() {
        this.app.set('port', this.port || process.env.PORT || 3030);
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
        this.app.use(body_parser_1.default.json());
    }
    routes() {
        this.app.use('/employees', routes_1.employeesRouter);
        this.app.use('/', routes_1.authRouter);
    }
    listen() {
        const port = this.app.get('port');
        this.app.listen(port);
        console.log('Server running on port', port);
    }
}
exports.App = App;
//# sourceMappingURL=app.js.map