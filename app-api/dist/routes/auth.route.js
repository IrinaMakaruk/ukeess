"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_controller_1 = require("../controllers/auth.controller");
const router = express_1.Router();
exports.authRouter = router;
router.post('/login', auth_controller_1.Auth);
router.post('/register', auth_controller_1.Registration);
;
//# sourceMappingURL=auth.route.js.map