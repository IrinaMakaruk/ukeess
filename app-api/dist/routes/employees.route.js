"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const employees_controller_1 = require("../controllers/employees.controller");
const jwt_1 = require("../middlware/jwt");
const router = express_1.Router();
exports.employeesRouter = router;
router.use(jwt_1.authenticateToken);
router.get('/pagination/:page', jwt_1.authenticateToken, employees_controller_1.getEmployees);
router.get('/departments', employees_controller_1.getDepartments);
router.get('/search/:searchParam*?', employees_controller_1.searchEmployee);
router.get('/count', employees_controller_1.getEmployeesCount);
router.delete('/:empId', jwt_1.authenticateToken, employees_controller_1.deleteEmployee);
router.put('/:empId', jwt_1.authenticateToken, employees_controller_1.updateEmployee);
router.post('/add', jwt_1.authenticateToken, employees_controller_1.addEmployee);
//# sourceMappingURL=employees.route.js.map