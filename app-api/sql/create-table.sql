-- Copy script bellow and run in sql 

DROP DATABASE IF EXISTS sql_app;

CREATE DATABASE sql_app;

DROP TABLE IF EXISTS sql_app.Departments;

CREATE TABLE sql_app.Departments (
    dpID INT(10) NOT NULL UNIQUE AUTO_INCREMENT,
    dpName VARCHAR(255)
);

describe sql_app.Departments;

DROP TABLE IF EXISTS sql_app.Employees;

CREATE TABLE sql_app.Employees (
    empId INT NOT NULL AUTO_INCREMENT,
    empName VARCHAR(255),
    empActive BIT,
    empDpID INT(10),
    PRIMARY KEY (empId),
    FOREIGN KEY (empDpID) REFERENCES sql_app.Departments (dpID)
);

describe sql_app.Employees;

-- Fake data for Departments table
INSERT INTO sql_app.Departments (dpName)
VALUES ('IT'), ('HR'), ('SALES');
-- Fake employees data
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Nissim Sharp",true,1),("Melinda Lester",true,2),("Ayanna Reese",false,3),("Yeo Gardner",true,3),("Quinn Potter",false,2),("Reese Harvey",false,2),("Murphy Mcintyre",false,3),("Kylan Manning",false,1),("Keefe Johns",true,3),("Patrick Meyers",false,1);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Hiram Fuentes",false,1),("Kathleen Chambers",false,1),("Buffy Simpson",false,3),("Keegan Weiss",true,1),("Drew Hebert",false,1),("Brandon Mccall",false,1),("Declan Britt",true,1),("Sheila Santiago",true,1),("Meredith Dotson",true,1),("Ora Langley",true,2);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Hiram Fuentes",false,1),("Kathleen Chambers",false,1),("Buffy Simpson",false,3),("Keegan Weiss",true,1),("Drew Hebert",false,1),("Brandon Mccall",false,1),("Declan Britt",true,1),("Sheila Santiago",true,1),("Meredith Dotson",true,1),("Ora Langley",true,2);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Hiram Fuentes",false,1),("Kathleen Chambers",false,1),("Buffy Simpson",false,3),("Keegan Weiss",true,1),("Drew Hebert",false,1),("Brandon Mccall",false,1),("Declan Britt",true,1),("Sheila Santiago",true,1),("Meredith Dotson",true,1),("Ora Langley",true,2);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Evan Franklin",false,1),("Lesley Rosa",false,3),("Beck Gay",true,2),("Colt Dillard",true,3),("Nichole Finch",false,1),("Ali Johns",true,1),("Cruz Hyde",true,1),("Hiroko Lara",true,2),("Chastity Rosales",false,3),("Cora Golden",true,3);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Garth Bernard",false,2),("Britanney Patel",false,2),("Simon Moses",true,2),("Beatrice Lawson",true,3),("Galvin Diaz",true,2),("Zahir Wolf",true,1),("Ralph Tillman",true,2),("Ciaran Whitfield",true,2),("Mary Davis",true,2),("Hedy Kirkland",false,2);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Galvin Fulton",false,3),("Travis Mcbride",false,2),("Lani Waters",false,3),("Jordan Odom",false,2),("Mollie Larson",true,3),("Irene Carrillo",true,1),("Raymond Mccarthy",false,1),("Cooper Boyer",true,3),("Abbot Baird",false,3),("Glenna Wells",false,2);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Marvin Richardson",true,3),("Mara Dominguez",false,2),("Seth Gardner",true,3),("Craig Harrell",false,1),("Aphrodite Christensen",false,1),("Dominique Barrett",false,2),("Melodie Burgess",false,2),("Mufutau Decker",true,2),("Kamal Rasmussen",true,2),("Price Watson",false,3);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("David Clay",true,1),("Maite Morton",false,2),("Elijah Lancaster",false,1),("Meghan Johnson",false,2),("Sawyer Buckner",false,3),("Alyssa Norman",false,1),("Hiroko Davenport",true,1),("Wallace Baird",true,1),("Dylan David",true,3),("Declan Chase",true,3);
INSERT INTO sql_app.Employees (empName,empActive,empDpID) VALUES ("Susan Atkins",false,2),("Aaron Horn",true,2),("Rudyard Munoz",false,1),("Gretchen Crawford",false,3),("Abbot Gibson",true,2),("Todd Marsh",true,1),("Claire Mcintosh",false,1),("Adele Boyd",false,2),("Hedwig Potts",true,3),("Jade Contreras",false,3);



-- AUTH

CREATE TABLE IF NOT EXISTS sql_app.Accounts (
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(255) NOT NULL,
  email VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE sql_app.Accounts ADD PRIMARY KEY (`id`);
ALTER TABLE sql_app.Accounts MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

-- add row to accounts table
INSERT INTO sql_app.Accounts (`id`, `username`, `password`, `email`) VALUES (1, 'admin', 'admin', 'admin@gmail.com');

-- add index on employees table
CREATE INDEX emp_name ON sql_app.Employees (empName);

-- select row by department name
SELECT * FROM sql_app.Departments WHERE dpName ='HR';

-- insert into employee table
INSERT INTO sql_app.Employees (empName, empActive, empDpID)
VALUES ('Makaruk', true, 1);

-- delete from employee table
DELETE FROM sql_app.Employees
WHERE empName = 'Korotych';

-- edit employee
UPDATE sql_app.Employees
SET empName = 'Korotych', empActive = true, empDpID = 1
WHERE empid = 2;

-- search employee
SELECT * FROM sql_app.Employees USE INDEX (emp_name) WHERE empName like 'A%';