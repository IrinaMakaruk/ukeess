import { App } from './app';

function main() {
    const app = new App( 3030 );
    app.listen();
}

main();
