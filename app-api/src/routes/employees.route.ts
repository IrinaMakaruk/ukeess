import { Router } from 'express'
import { 
    getEmployees,
    addEmployee,
    searchEmployee,
    deleteEmployee,
    updateEmployee, 
    getEmployeesCount,
    getDepartments
} from '../controllers/employees.controller'
import  { authenticateToken } from '../middlware/jwt';

const router = Router();

router.use(authenticateToken);

router.get('/pagination/:page', authenticateToken, getEmployees);
router.get('/departments', getDepartments);
router.get('/search/:searchParam*?', searchEmployee);
router.get('/count', getEmployeesCount);

router.delete('/:empId', authenticateToken, deleteEmployee);
router.put('/:empId', authenticateToken, updateEmployee);

router.post('/add', authenticateToken, addEmployee);


export { router as employeesRouter };