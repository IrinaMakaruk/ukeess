import { Router } from 'express'

import { Auth, Registration } from '../controllers/auth.controller';

const router = Router();

router.post('/login', Auth);
router.post('/register', Registration);

export { router as authRouter };;