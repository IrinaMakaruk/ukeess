import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken';
import { User } from '../interface/user.interface';

export const secret = 'secretkey23456';

export function generateAccessToken(user: User) {
    return jwt.sign(user, secret);
}

export function authenticateToken(req: Request, res: Response, next: NextFunction) {
    try {
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1];
        if (token == null)  {
            return res.sendStatus(401);
        } 
        
        const decoded = jwt.verify( token, secret) ;
        req.user = decoded;
        res.cookie('SESSIONID', token, {httpOnly:true, secure:true});
        next();
    } catch (err) {
        return res.status(401).send({
            msg: 'You are not logged in. Please login again'
        });
    }
  }
