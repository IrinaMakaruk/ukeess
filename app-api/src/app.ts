import express, { Application } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { 
    employeesRouter,
    authRouter
} from './routes'

export class App {
    private app: Application;
    constructor( private port?: number | string ) {
        this.app = express();
        this.app.use( cors() );
        this.setAppSettings();
        this.routes();
    }

    private setAppSettings() {
        this.app.set( 'port', this.port || process.env.PORT || 3030 );
        this.app.use(bodyParser.urlencoded({extended : true}));
        this.app.use(bodyParser.json());
    }

    private routes() {
        this.app.use('/employees', employeesRouter);
        this.app.use('/', authRouter);
    }

    listen() {
        const port = this.app.get( 'port');
        this.app.listen(port);
        console.log( 'Server running on port', port );
    }

}