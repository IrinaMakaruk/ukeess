import { createPool, Pool } from 'mysql';

export async function connect(): Promise<Pool> {
    return createPool({
        host: 'localhost',
        user: 'root',
        database: 'sql_app',
        connectionLimit: 100,
        typeCast: (field, next) => {
            if (field.type === 'BIT' && field.length == 1) {
                return( field.buffer()[ 0 ] === 1 );
            }
            return next();
        }
    })
}