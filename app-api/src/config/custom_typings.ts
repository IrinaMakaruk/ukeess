declare namespace Express {
    interface Request {
        user: string | object;
        name: string | object;
    }
}