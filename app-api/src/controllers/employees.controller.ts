import { Request, Response } from 'express'
import { connect } from '../config/database'
import { Employee } from '../interface/employee.interface'

export async function getEmployees(req: Request, res: Response): Promise<Response | void> {
    try {
        const connection = await connect();
        const pageNumber = Number(req.params.page)
        const offset = 10 * ( pageNumber -1 );

        connection.query('SELECT sql_app.Employees.*, sql_app.Departments.dpName from sql_app.Employees INNER JOIN sql_app.Departments ON sql_app.Employees.empDpID = sql_app.Departments.dpID ORDER BY empId LIMIT 10 OFFSET ?', [offset], (err, employees) => {
            if (err) throw err;
            res.json(employees)
        });
    }
    catch (e) {
        console.error(e)
    }
}

export async function addEmployee(req: Request, res: Response) {
    try {
        const newEmployee: Employee = req.body;
        const connection = await connect();
        connection.query('INSERT INTO sql_app.Employees SET ?', [newEmployee], err => {
            if (err) throw err;
            res.json({
                message: 'new Employee added'
            });
        });
    }
    catch (e) {
        console.error(e)
    }
}

export async function searchEmployee(req: Request, res: Response) {
    try {
        const searchParam = req.params.searchParam;
        const connection = await connect();
        connection.query('SELECT sql_app.Employees.*, sql_app.Departments.dpName from sql_app.Employees JOIN sql_app.Departments ON sql_app.Employees.empDpID = sql_app.Departments.dpID WHERE empName like ? ORDER BY empId', [`${searchParam}%`], (err, employees) => {
            if (err) throw err;
            res.send(employees)
        });
    }
    catch (e) {
        console.error(e)
    }
}

export async function deleteEmployee(req: Request, res: Response) {
    try {
        const id = req.params.empId;
        const connection = await connect();
        connection.query('DELETE FROM sql_app.Employees WHERE empId = ?', [id], (err) => {
            if (err) throw err;
            res.json({
                message: 'Employee Deleted'
            });
        });
    }
    catch (e) {
        console.error(e)
    }
}

export async function updateEmployee(req: Request, res: Response) {
    try {
        const id = req.params.empId;
        const updateEmployee: Employee = req.body;
        const connection = await connect();
        connection.query('UPDATE sql_app.Employees SET ? WHERE empId = ?', [updateEmployee, id], (err) => {
            if (err) throw err;
            res.json({
                message: 'Employee Updated'
            });
        });
    }
    catch (e) {
        console.error(e)
    }
}

export async function getEmployeesCount(_req: Request, res: Response) {
    try {   
        const connection = await connect();
        connection.query('SELECT COUNT(DISTINCT empId) AS numbers FROM sql_app.Employees', (err, count) => {
            if (err) throw err;
            res.json(count[0].numbers)
        });
    }
    catch (e) {
        console.error(e)
    }
}

export async function getDepartments(_req: Request, res: Response) {
    try {
        const connection = await connect();
        connection.query('SELECT * FROM sql_app.Departments', (err, departments) => {
            if (err) throw err;
            res.json(departments)
        });
    }
    catch (e) {
        console.error(e)
    }
}