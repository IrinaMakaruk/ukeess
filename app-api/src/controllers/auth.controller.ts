import { Request, Response } from 'express'
import bcrypt from 'bcryptjs';
import { connect } from '../config/database'
import { generateAccessToken } from '../middlware/jwt';

const notLogged = (username: string, password: string) => !username && !password;

export async function Registration( req: Request, res: Response): Promise<Response | void> {
    try {
        const connection = await connect();
        connection.query(`SELECT * FROM sql_app.Accounts WHERE LOWER(username) = LOWER(
            ${connection.escape(req.body.username)});`,
            (err, result) => {

                if (result.length) {
                    return res.status(409).send({
                        msg: 'This username is already in use!'
                    });
                } else {
                // username is available
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).send({msg: "Error happend on the server. Please try later"});
                    } else {
                    // has hashed pw => add to database
                    connection.query(
                        `INSERT INTO sql_app.Accounts (username, email, password) VALUES (
                            ${connection.escape(req.body.username)},
                            ${connection.escape(req.body.email)},
                            ${connection.escape(hash)})`,
                        (err, result) => {
                            if (err) return res.status(400).send({msg: err});

                            const loggedUser = {
                                username: req.body.username,
                                password: req.body.password
                            };

                            const token = generateAccessToken( loggedUser );
                            return res.status(201).send({
                                msg: 'Registered!',
                                token,
                                user: loggedUser
                            });
                        }
                    );
                }
                });
            }
          })
    }
    catch (e) {
        console.error(e)
    }
}

export async function Auth ( req: Request, res: Response) {
    try {
        const connection = await connect();
        const { username, password } = req.body;
        if ( notLogged(username, password)) return res.status(401).send({msg: 'User not logged!'});

        connection.query(`SELECT * FROM sql_app.Accounts WHERE username = ${connection.escape(username)}`, (error, results) => {
            // user does not exists
            if (error) {
                return res.status(400).send({
                    msg: error
                });
            }
            if (!results.length) {
                return res.status(401).send({
                    msg: 'Username or password is incorrect!'
                });
            }
            // check password
            const loggedUser = results[0];
            bcrypt.compare( password, loggedUser.password, (loginError, loginResult) => {
                // wrong password
                if (loginError) {
                    return res.status(401).send({
                        msg: 'Username or password is incorrect!'
                    });
                }
                if (loginResult) {
                    const token = generateAccessToken({username: loggedUser.username, password: loggedUser.password});
                    return res.status(200).send({
                        msg: 'Logged in!',
                        token,
                        user: loggedUser
                    });
                }
                return res.status(401).send({
                    msg: 'Username or password is incorrect!'
                });
            });
        }
    )}
    catch (e) {
        console.error(e)
    }
}
