export interface Employee {
    empId?: string;
    empName: string;
    empActive: boolean;
    empDpID: number
}